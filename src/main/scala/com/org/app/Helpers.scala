package com.org.app

import java.io.{File, PrintWriter}
import java.util.concurrent.TimeUnit

import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.SparkContext
import org.apache.spark.sql.functions.{array, col, explode}
import org.apache.spark.sql.{SQLContext, SaveMode}
import org.mongodb.scala._
import org.mongodb.scala.model.Projections._

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future, Promise}

object Helpers {

  implicit class DocumentObservable[C](val observable: Observable[Document]) extends ImplicitObservable[Document] {
    override val converter: (Document) => String = (doc) => doc.toJson
  }

  implicit class GenericObservable[C](val observable: Observable[C]) extends ImplicitObservable[C] {
    override val converter: (C) => String = (doc) => doc.toString
  }

  trait ImplicitObservable[C] {
    val observable: Observable[C]
    val converter: (C) => String

    def results(): Seq[C] = Await.result(observable.toFuture(), Duration(10, TimeUnit.SECONDS))
    def headResult() = Await.result(observable.head(), Duration(10, TimeUnit.SECONDS))
    def printResults(initial: String = ""): Unit = {
      if (initial.length > 0) print(initial)
      results().foreach(res => println(converter(res)))
    }
    def printHeadResult(initial: String = ""): Unit = println(s"${initial}${converter(headResult())}")
  }



  def exportJSONCSVFlatten(collection: MongoCollection[Document], output: PrintWriter, s:String,sc:SparkContext){
    val promise = Promise[Completed]()
    var cursor=collection.find()
    val sqlContext = new SQLContext(sc)

    if(s.equals("custommetric")){
      cursor = collection.find().projection(fields(include("_id", "title","dummyFormula","idsMap")))

    }
    else if(s.equals("metric")){
      cursor = collection.find().projection(fields(include("_id", "title","metricId")))
    }
    else if(s.equals("dimension")){
      cursor = collection.find().projection(fields(include("_id", "title","dimId")))
    }
    println("Total Records in "+s+" "+cursor.results().size)

    if(cursor.results().size>0){
        cursor.subscribe(
          (doc: Document) => output.println(doc.toJson()),
          (t: Throwable) => promise.failure(t),
          () => {

            output.close()
            promise.success(Completed())
          }
        )
        showPinWheel(promise.future)
        val df = sqlContext.read.json("temp_"+s+".json")



        if(s.equals("custommetric")) {
          val d=df.select(col("_id"),
                col("idsmap.*"),
                col("title"))

          d.show()

          if(d.columns.contains("E")){
            val dwdJson_e = df.withColumn("Sigview_Name", col("_id"))
              .withColumn("dummyFormula", col("dummyFormula"))
              .withColumn("idsMap", explode(array(col("idsMap"))))
              .withColumn("A", col("idsMap.A"))
              .withColumn("B", col("idsMap.B"))
              .withColumn("C", col("idsMap.C"))
              .withColumn("D", col("idsMap.D"))
              .withColumn("E", col("idsMap.E"))
              .withColumn("title", col("title"))
              .withColumn("E", col("idsMap.E"))
              .withColumn("E", explode(array(col("E"))))
              .withColumn("E_m_id", col("E.id"))
              .withColumn("E_function", col("E.function"))
              .withColumn("A", explode(array(col("A"))))
              .withColumn("A_m_id", col("A.id"))
              .withColumn("A_function", col("A.function"))
              .withColumn("B", explode(array(col("B"))))
              .withColumn("B_m_id", col("B.id"))
              .withColumn("B_function", col("B.function"))
              .withColumn("C", explode(array(col("C"))))
              .withColumn("C_m_id", col("C.id"))
              .withColumn("C_function", col("C.function"))
              .withColumn("D", explode(array(col("D"))))
              .withColumn("D_m_id", col("D.id"))
              .withColumn("D_function", col("D.function"))

            val dwdJson_e1=dwdJson_e.select(col("_id").as("Sigview_Name"),
              col("dummyFormula"),
              col("A_m_id"),
              col("B_m_id"),
              col("C_m_id"),
              col("D_m_id"),
              col("E_m_id"),
              col("A_function"),
              col("B_function"),
              col("C_function"),
              col("D_function"),
              col("E_function"),
              col("title"))

            dwdJson_e1.show()
            dwdJson_e1.write.mode(SaveMode.Overwrite).format("com.databricks.spark.csv").option("header", "true").csv(s)
          }
          else if(d.columns.contains("D")){
            val dwdJson_e = df.withColumn("Sigview_Name", col("_id"))
              .withColumn("dummyFormula", col("dummyFormula"))
              .withColumn("idsMap", explode(array(col("idsMap"))))
              .withColumn("A", col("idsMap.A"))
              .withColumn("B", col("idsMap.B"))
              .withColumn("C", col("idsMap.C"))
              .withColumn("D", col("idsMap.D"))
              .withColumn("title", col("title"))
              .withColumn("A", explode(array(col("A"))))
              .withColumn("A_m_id", col("A.id"))
              .withColumn("A_function", col("A.function"))
              .withColumn("B", explode(array(col("B"))))
              .withColumn("B_m_id", col("B.id"))
              .withColumn("B_function", col("B.function"))
              .withColumn("C", explode(array(col("C"))))
              .withColumn("C_m_id", col("C.id"))
              .withColumn("C_function", col("C.function"))
              .withColumn("D", explode(array(col("D"))))
              .withColumn("D_m_id", col("D.id"))
              .withColumn("D_function", col("D.function"))

            val dwdJson_e1=dwdJson_e.select(col("_id").as("Sigview_Name"),
              col("dummyFormula"),
              col("A_m_id"),
              col("B_m_id"),
              col("C_m_id"),
              col("D_m_id"),
              col("A_function"),
              col("B_function"),
              col("C_function"),
              col("D_function"),
              col("title"))
            dwdJson_e1.show()
            dwdJson_e1.write.mode(SaveMode.Overwrite).format("com.databricks.spark.csv").option("header", "true").csv(s)
          }
          else{
            val dwdJson_e = df.withColumn("Sigview_Name", col("_id"))
              .withColumn("dummyFormula", col("dummyFormula"))
              .withColumn("idsMap", explode(array(col("idsMap"))))
              .withColumn("A", col("idsMap.A"))
              .withColumn("B", col("idsMap.B"))
              .withColumn("C", col("idsMap.C"))
              .withColumn("title", col("title"))
              .withColumn("A", explode(array(col("A"))))
              .withColumn("A_m_id", col("A.id"))
              .withColumn("A_function", col("A.function"))
              .withColumn("B", explode(array(col("B"))))
              .withColumn("B_m_id", col("B.id"))
              .withColumn("B_function", col("B.function"))
              .withColumn("C", explode(array(col("C"))))
              .withColumn("C_m_id", col("C.id"))
              .withColumn("C_function", col("C.function"))

            val dwdJson_e1=dwdJson_e.select(col("_id").as("Sigview_Name"),
              col("dummyFormula"),
              col("A_m_id"),
              col("B_m_id"),
              col("C_m_id"),
              col("A_function"),
              col("B_function"),
              col("C_function"),
              col("title"))
            dwdJson_e1.show()
            dwdJson_e1.write.mode(SaveMode.Overwrite).format("com.databricks.spark.csv").option("header", "true").csv(s)
          }


        }

        else if(s.equals("metric")) {
          val dwdJson_e1=df.select(col("_id").as("Sigview_Name"),
            col("metricId").as("Nitrodb_Name"),
            col("title"))
          dwdJson_e1.show()
          dwdJson_e1.write.mode(SaveMode.Overwrite).format("com.databricks.spark.csv").option("header", "true").csv(s)
        }
        else if(s.equals("dimension")){
          val dwdJson_e1=df.select(col("_id").as("Sigview_Name"),
            col("dimId").as("Nitrodb_Name"),
            col("title"))
          dwdJson_e1.show()
          dwdJson_e1.write.mode(SaveMode.Overwrite).format("com.databricks.spark.csv").option("header", "true").csv(s)
        }
        val fs = FileSystem.get(sc.hadoopConfiguration)
        val filePath = s+"/"
        val fileName = fs.globStatus(new Path(filePath+"part*"))(0).getPath.getName
        fs.rename(new Path(filePath+fileName), new Path(s+".csv"))
    }
    else{
      val f:File=new File(s+".csv")
      if(f.exists()){
        f.delete()
      }
    }


    val f:File=new File("temp_"+s+".json")

    f.delete()


  }


  def exportJson(collection: MongoCollection[Document], output: PrintWriter,s:String){
    val promise = Promise[Completed]()

    var cursor=collection.find()
    if(s.equals("custommetric")){
      cursor = collection.find().projection(fields(include("_id", "title","dummyFormula","idsMap")))
    }
    else if(s.equals("metric")){
      cursor = collection.find().projection(fields(include("_id", "title","metricId")))
    }
    else if(s.equals("dimension")){
      cursor = collection.find().projection(fields(include("_id", "title","dimId")))
    }


    if(cursor !=null){
      output.println("[")
      cursor.subscribe(
        (doc: Document) => output.println(doc.toJson()+","),
        (t: Throwable) => promise.failure(t),
        () => {
          output.println("{}]")

          output.close()
          promise.success(Completed())
        }
      )
      showPinWheel(promise.future)
    }

  }

  def showPinWheel(someFuture: Future[_]) {
    // Let the user know something is happening until futureOutput isCompleted
    val spinChars = List("|", "/", "-", "\\")
    while (!someFuture.isCompleted) {
      spinChars.foreach({
        case char =>
          Console.err.print(char)
          Thread sleep 500
          Console.err.print("\b")
      })
    }
    Console.err.println("")
  }

}
