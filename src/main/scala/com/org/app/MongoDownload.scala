package com.org.app

import java.io.PrintWriter

import com.org.app.Helpers._
import org.apache.spark.{SparkConf, SparkContext}
import org.mongodb.scala.{MongoException, _}

object MongoDownload{

  def main(args: Array[String]): Unit = {

//    val org="openx"
//    val view_type="default"
//    val download_type="CSV"
    val org=args(0)
    val view_type=args(1)
    val download_type=args(2)

    createFiles(org,view_type,download_type)

  }

  def createFiles(org:String, view_type:String, download_type:String): Unit ={

    try {

      val mongoClient: MongoClient = getMongoConnection()
      val database: MongoDatabase = mongoClient.getDatabase(org)

      val custommetric: MongoCollection[Document] = database.getCollection("custommetric_"+view_type)
      val metric: MongoCollection[Document] = database.getCollection("metric_"+view_type)
      val dimension: MongoCollection[Document] = database.getCollection("dimension_"+view_type)

      if(download_type.toUpperCase.equals("CSV")){
        val conf = new SparkConf().setAppName("Simple Application").setMaster("local")
        val sc = new SparkContext(conf)


        val output: PrintWriter = new PrintWriter("temp_custommetric.json")
        val output1: PrintWriter = new PrintWriter("temp_metric.json")
        val output2: PrintWriter = new PrintWriter("temp_dimension.json")

        exportJSONCSVFlatten(custommetric, output,"custommetric",sc)
        exportJSONCSVFlatten(metric, output1,"metric",sc)
        exportJSONCSVFlatten(dimension, output2,"dimension",sc)

        sc.stop()
      }

      else if(download_type.toUpperCase.equals("JSON")){
        val output: PrintWriter = new PrintWriter("custommetric.json")
        val output1: PrintWriter = new PrintWriter("metric.json")
        val output2: PrintWriter = new PrintWriter("dimension.json")

        exportJson(custommetric, output,"custommetric")
        exportJson(metric, output1,"metric")
        exportJson(dimension, output2,"dimension")
      }

      mongoClient.close()
    }
    catch {
      case mongoexcp: MongoException => mongoexcp.printStackTrace
      case e: Exception => e.printStackTrace
    }
    System.out.println();
  }
  def getMongoConnection():MongoClient={
    MongoClient("mongodb://172.30.0.52:27017")
//    MongoClient("mongodb://52.91.215.186:27017")
  }

}






