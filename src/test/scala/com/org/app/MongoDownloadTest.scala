package com.org.app

import org.mongodb.scala.{MongoClient, MongoDatabase}
import org.scalatest.{BeforeAndAfter, FunSuite, Matchers}
import Helpers._


class MongoDownloadTest extends FunSuite with Matchers with BeforeAndAfter{

  val conn:MongoClient=MongoDownload.getMongoConnection()
  val database: MongoDatabase = conn.getDatabase("openx")
  test("test connection with mongodb custom metric") {

    database should not be null
    val custommetric = database.getCollection("custommetric_default").find().results()
    custommetric.size should be (18)
  }

  test("test connection with mongodb metric") {
    val metric = database.getCollection("metric_default").find().results()
    metric.size should be (18)

  }

  test("test connection with mongodb dimension") {
    val dimension = database.getCollection("dimension_default").find().results()
    dimension.size should be (62)
  }

}
